package com.rest.test;

import com.rest.client.ParkingClient;
import com.rest.model.ParkingPlace;
import org.apache.log4j.Logger;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.List;

public class ParkingServiceTest {
    private static Logger LOG = Logger.getLogger(ParkingServiceTest.class);
    private static ParkingClient CLIENT = new ParkingClient();
    private static String LIBRARY_URL = "http://localhost:9999/rest/service/parking";
    private SoftAssert verify;

    @BeforeMethod
    public void setUp() {
        verify = new SoftAssert();
    }

    @Test
    public void getAllPlacesSuccessTest() {
        LOG.info("Getting all places and verifying it's size");
        LOG.info("Getting response from service...");
        List<ParkingPlace> actualResult = CLIENT.get(LIBRARY_URL);
        LOG.info("Response is null: " + (actualResult == null));
        verify.assertNotNull(actualResult);
        LOG.info("Response size is 10: " + (actualResult.size() == 10));
        verify.assertEquals(actualResult.size(), 10);
    }

    @Test
    public void getVacantPlacesSuccessTest() {
        LOG.info("Getting all vacant places and verifying it's size");
        LOG.info("Getting response from service...");
        List<ParkingPlace> actualResult = CLIENT.get(LIBRARY_URL + "/vacant");
        LOG.info(String.format("Result has been received. Results: %s", actualResult));
        LOG.info("Response is not null: " + (actualResult != null));
        verify.assertNotNull(actualResult);
        LOG.info("Response size is 3: " + (actualResult.size() == 3));
        verify.assertEquals(actualResult.size(), 3);
    }

    @Test
    public void getVacantPlacesFailureTest() {
        LOG.info("Getting all vacant places when no vacant places are available");
        LOG.info("Taking all vacant places");
        CLIENT.put(LIBRARY_URL + "/takePlace", new ParkingPlace("AS3456TR"));
        CLIENT.put(LIBRARY_URL + "/takePlace", new ParkingPlace("YU5678TY"));
        CLIENT.put(LIBRARY_URL + "/takePlace", new ParkingPlace("AE7693TR"));

        LOG.info("Getting response from service...");
        List<ParkingPlace> actualResponse = CLIENT.get(LIBRARY_URL + "/vacant");
        LOG.info(String.format("Actual response '%s'", actualResponse));
        LOG.info("Response is equal null: " + (actualResponse == null));
        verify.assertNull(actualResponse);

        LOG.info("Freeing taken places");
        CLIENT.put(LIBRARY_URL + "/freePlace", new ParkingPlace(2));
        CLIENT.put(LIBRARY_URL + "/freePlace", new ParkingPlace(3));
        CLIENT.put(LIBRARY_URL + "/freePlace", new ParkingPlace(6));
    }

    @Test
    public void getPlaceByIdSuccessTest() {
        LOG.info("Getting place by id and verifying it");
        ParkingPlace expectedResult = new ParkingPlace("Leopolis", 4, 13, false, "AB1594BR", "01.05.2017", "02:49");
        LOG.info(String.format("Expected result '%s'", expectedResult.toString()));
        LOG.info("Getting response from service...");
        String actualResult = CLIENT.getById(LIBRARY_URL + "/4");
        LOG.info(String.format("Result has been received. Results: %s", actualResult));
        LOG.info("Expected and actual results are equal: " + (actualResult.equals(expectedResult.toString())));
        verify.assertEquals(actualResult, expectedResult);
    }

    @Test
    public void getPlaceByNotValidIdFailureTest() {
        LOG.info("Getting place by not valid id and verifying response result");
        String expectedResult = "{  \"Message\": \"Incorrect input value  [Id: -2]\",  \"Title\": \"Failure\",  \"Code\": \"400 - Bad Request\"}";
        LOG.info(String.format("Expected result '%s'", expectedResult));
        LOG.info("Getting response from service...");
        String actualResult = CLIENT.getById(LIBRARY_URL + "/-2");
        LOG.info(String.format("Actual result '%s'", actualResult));
        LOG.info("Expected and actual results are equal: " + (actualResult.equals(expectedResult)));
        verify.assertEquals(actualResult, expectedResult);
    }

    @Test
    public void getPlaceByNotExistingIdFailureTest() {
        LOG.info("Getting place by not existing id and verifying response result");
        LOG.info("Expected result 'null'");
        LOG.info("Getting response from service...");
        String actualResult = CLIENT.getById(LIBRARY_URL + "/78");
        LOG.info("Actual results is equal null");
        verify.assertNull(actualResult);
    }

    @Test
    public void takePlaceSuccessTest() {
        LOG.info("Taking vacant place on parking and verifying response result");
        String expectedResult = "{\"Message\":\"Parking place 2 was taken successfully by vehicle AS3456TR\"}";
        LOG.info(String.format("Expected result '%s'", expectedResult));
        LOG.info("Getting response from service...");
        String actualResult = CLIENT.put(LIBRARY_URL + "/takePlace", new ParkingPlace("AS3456TR"));
        LOG.info(String.format("Actual result '%s'", actualResult));
        LOG.info("Expected and actual results are equal: " + (actualResult.equals(expectedResult)));
        verify.assertEquals(actualResult, expectedResult);
        LOG.info("Freeing of taken place");
        CLIENT.put(LIBRARY_URL + "/freePlace", new ParkingPlace(2));
    }

    @Test
    public void takePlaceFailureTest() {
        LOG.info("Trying to take a place on parking when no vacant places are available");
        LOG.info("Taking all vacant places");
        CLIENT.put(LIBRARY_URL + "/takePlace", new ParkingPlace("AS3456TR"));
        CLIENT.put(LIBRARY_URL + "/takePlace", new ParkingPlace("YU5678TY"));
        CLIENT.put(LIBRARY_URL + "/takePlace", new ParkingPlace("AE7693TR"));

        LOG.info("Getting response from service...");
        String actualResult = CLIENT.put(LIBRARY_URL + "/takePlace", new ParkingPlace("AS3456YR"));
        LOG.info("Actual results is equal null: " + (actualResult == null));
        verify.assertNull(actualResult);

        LOG.info("Freeing of taken place");
        CLIENT.put(LIBRARY_URL + "/freePlace", new ParkingPlace(2));
        CLIENT.put(LIBRARY_URL + "/freePlace", new ParkingPlace(3));
        CLIENT.put(LIBRARY_URL + "/freePlace", new ParkingPlace(6));
    }

    @Test
    public void freeNotVacantPlaceSuccessTest() {
        LOG.info("Freeing place on parking and checks response result");
        String expectedResult = "{\"Message\":\"Parking place 1 has just been vacated\"}";
        LOG.info(String.format("Expected result '%s'", expectedResult));
        LOG.info("Getting response from service...");
        String actualResult = CLIENT.put(LIBRARY_URL + "/freePlace", new ParkingPlace(1));
        LOG.info(String.format("Actual result '%s'", actualResult));
        LOG.info("Expected and actual results are equal: " + (actualResult.equals(expectedResult)));
        verify.assertEquals(actualResult, expectedResult);
        LOG.info("Taking vacant place that we has been freed before");
        CLIENT.put(LIBRARY_URL + "/takePlace", new ParkingPlace("AB1234BA"));
    }

    @Test
    public void freeVacantPlaceSuccessTest() {
        LOG.info("Freeing vacant place on parking and checks response result");
        String expectedResult = "{\"Message\":\"Parking place 2 was free before trying to make it vacant\"}";
        LOG.info(String.format("Expected result '%s'", expectedResult));
        LOG.info("Getting response from service...");
        String actualResult = CLIENT.put(LIBRARY_URL + "/freePlace", new ParkingPlace(2));
        LOG.info(String.format("Actual result '%s'", actualResult));
        LOG.info("Expected and actual results are equal: " + (actualResult.equals(expectedResult)));
        verify.assertEquals(actualResult, expectedResult);
    }

    @Test
    public void freePlaceFailureTest() {
        LOG.info("Freeing place by not valid id and checks response result");
        String expectedResult = "{  \"Message\": \"Incorrect input value  [Id: -2]\",  \"Title\": \"Failure\",  \"Code\": \"400 - Bad Request\"}";
        LOG.info(String.format("Expected result '%s'", expectedResult));
        LOG.info("Getting response from service...");
        String actualResult = CLIENT.put(LIBRARY_URL + "/freePlace", new ParkingPlace(-2));
        LOG.info(String.format("Actual result '%s'", actualResult));
        LOG.info("Expected and actual results are equal: " + (actualResult.equals(expectedResult)));
        verify.assertEquals(actualResult, expectedResult);
    }
}
