package com.rest.client;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.rest.model.ParkingPlace;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.log4j.Logger;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class ParkingClient {
    private static Logger LOG = Logger.getLogger(ParkingClient.class);

    public List<ParkingPlace> get(String url) {
        LOG.info(String.format("Get method by url '%s' started", url));
        WebClient client = WebClient.create(url, Collections.singletonList(new JacksonJsonProvider()));
        LOG.info("Client has been created");
        client.accept(MediaType.APPLICATION_JSON);
        LOG.info("Setting accept 'application/json'");
        Response response = client.get();
        if (response.getStatus() == 204) {
            LOG.info(String.format("Results hasn't been received. Status: %s", response.getStatus()));
            return null;
        }
        LOG.info(String.format("Results has been received. Response: %s", response.toString()));
        return (List<ParkingPlace>) response.readEntity(List.class);
    }

    public String getById(String url) {
        LOG.info(String.format("Get method by url '%s' started", url));
        WebClient client = WebClient.create(url, Collections.singletonList(new JacksonJsonProvider()));
        LOG.info("Client has been created");
        client.accept(MediaType.APPLICATION_JSON);
        LOG.info("Setting accept 'application/json'");
        Response response = client.get();
        if (response.getStatus() != 200 && response.getStatus() != 204) {
            LOG.info(String.format("Results hasn't been received. Status: %s", response.getStatus()));
            return response.readEntity(Map.class).toString();
        } else if (response.getStatus() == 204) {
            LOG.info(String.format("Results hasn't been received. Status: %s", response.getStatus()));
            return null;
        }
        LOG.info(String.format("Results has been received. Response: %s", response.toString()));
        return response.readEntity(ParkingPlace.class).toString();
    }

    public String put(String url, ParkingPlace place) {
        LOG.info(String.format("Update method by url '%s' started", url));
        WebClient client = WebClient.create(url, Collections.singletonList(new JacksonJsonProvider()));
        client.type(MediaType.APPLICATION_JSON);
        LOG.info("Content-type 'application/json' has been set");
        client.accept(MediaType.APPLICATION_JSON);
        LOG.info("Accept 'application/json' has been set");
        Response response = client.put(place);
        if (response.getStatus() != 200 && response.getStatus() != 204) {
            LOG.info(String.format("Results hasn't been received. Status: %s", response.getStatus()));
            return response.readEntity(Map.class).toString();
        } else if (response.getStatus() == 204) {
            LOG.info(String.format("Results hasn't been received. Status: %s", response.getStatus()));
            return null;
        }
        LOG.info(String.format("Results has been received. Response: %s", response));
        return response.readEntity(String.class);
    }
}
