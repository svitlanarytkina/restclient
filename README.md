# README #

1. Run service: mvn clean package jetty:run-war
2. Methods:
	- get all places: http://localhost:9999/rest/service/parking
	- get all vacant places: http://localhost:9999/rest/service/parking/vacant
	- get place by id: http://localhost:9999/rest/service/parking/{id}
	- take place: http://localhost:9999/rest/service/parking/takePlace with json object containing vehicleRegistrationPlate
	- free place by id:  http://localhost:9999/rest/service/parking/freePlace with json object containing id
3. Run tests: mvn clean test site jetty:run