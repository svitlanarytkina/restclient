package com.rest.model;

public class ParkingPlace {
    private static String PARKING_MAIN_INFO = "Parking place #%s:\n\t\tParking - %s\n\t\tPrice per hour - %s $\n\t\tIs Available - \n\t\t";
    private static String PARKING_ADDITIONAL_INFO = "Registration plate - %s\n\t\tStart time - %s %s\n";

    private String parkingName;
    private int id;
    private int pricePerHour;
    private boolean isVacant;
    private String vehicleRegistrationPlate;
    private String startDate;
    private String startTime;

    public ParkingPlace() {
    }

    public ParkingPlace(int id) {
        this.id = id;
    }

    public ParkingPlace(String vehicleRegistrationPlate) {
        this.vehicleRegistrationPlate = vehicleRegistrationPlate;
    }

    public ParkingPlace(String parkingName, int id, int pricePerHour, boolean isVacant, String vehicleRegistrationPlate,
                        String startDate, String startTime) {
        this.parkingName = parkingName;
        this.id = id;
        this.pricePerHour = pricePerHour;
        this.isVacant = isVacant;
        this.vehicleRegistrationPlate = vehicleRegistrationPlate;
        this.startDate = startDate;
        this.startTime = startTime;
    }

    public String getParkingName() {
        return parkingName;
    }

    public void setParkingName(String parkingName) {
        this.parkingName = parkingName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPricePerHour() {
        return pricePerHour;
    }

    public void setPricePerHour(int pricePerHour) {
        this.pricePerHour = pricePerHour;
    }

    public boolean isVacant() {
        return isVacant;
    }

    public void setVacant(boolean vacant) {
        isVacant = vacant;
    }

    public String getVehicleRegistrationPlate() {
        return vehicleRegistrationPlate;
    }

    public void setVehicleRegistrationPlate(String vehicleRegistrationPlate) {
        this.vehicleRegistrationPlate = vehicleRegistrationPlate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    @Override
    public String toString() {
        return isVacant ? String.format(PARKING_MAIN_INFO, id, parkingName, pricePerHour, "Yes")
                : String.format(PARKING_MAIN_INFO, id, parkingName, pricePerHour, "No")
                .concat(String.format(PARKING_ADDITIONAL_INFO, vehicleRegistrationPlate, startDate, startTime));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParkingPlace place = (ParkingPlace) o;
        return id == place.id && pricePerHour == place.pricePerHour && isVacant == place.isVacant &&
                parkingName.equals(place.parkingName) && vehicleRegistrationPlate.equals(place.vehicleRegistrationPlate) &&
                startDate.equals(place.startDate) && startTime.equals(place.startTime);
    }

    @Override
    public int hashCode() {
        int result = parkingName.hashCode();
        result = 31 * result + id;
        result = 31 * result + pricePerHour;
        result = 31 * result + (isVacant ? 1 : 0);
        result = 31 * result + vehicleRegistrationPlate.hashCode();
        result = 31 * result + startDate.hashCode();
        result = 31 * result + startTime.hashCode();
        return result;
    }
}
